package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.Card;
import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.CardsAdapter;
import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.R;
import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.gamelogic.GameController;

public class CardViewHolder extends RecyclerView.ViewHolder {
    // each data item is just a string in this case
    public View mTextView;
    public Card card;
    public TextView player;
    private TextView nameTv, contentTv;
    private String name, content;
    private boolean inDeck, isOwned;
    public OnViewClickedListener mListener;

    public CardViewHolder(final View v, final OnViewClickedListener mListener, final GameController gameController) {
        super(v);
        this.mListener = mListener;
        mTextView = v;
        player = v.findViewById(R.id.textPlayer);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //Whenever a card is clicked, callback to GameActivity.java
                if (mListener != null) {
                    mListener.onViewClickedListener(getAdapterPosition(), gameController);
                }
            }
        });
    }

    public void bindCardToView(Card card){
        this.card = card;
        player.setText(this.card.getCategory());
    }

    public interface OnViewClickedListener{
        void onViewClickedListener(int position, GameController gameController);
    }
}