package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.Async.Images;

import static android.content.ContentValues.TAG;
import static edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.GameActivity.context;

/**
 * Created by anthonyvisalli on 3/21/18.
 */

public class PlayersAdapter extends RecyclerView.Adapter<PlayersAdapter.ViewHolder> {
    int numPlayers;
    int[] scores;
    String[] playerNames;
    boolean[] is_Judge;
    private static ImageView imageView;
    List<Images> imagesList;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View mTextView;
        public TextView player;
        public TextView score;

        public ViewHolder(View v) {
            super(v);
            mTextView = v;
            player = v.findViewById(R.id.textPlayer);
            score = v.findViewById(R.id.textScore);
            imageView = (ImageView) itemView
                    .findViewById(R.id.playerPic);
        }
        public void bindDrawable(Drawable drawable){
            imageView.setImageDrawable(drawable);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public PlayersAdapter(int numPlayers, int[] scores, String[] playerNames, boolean[] is_Judge, List<Images> imagesList) {
        this.scores = scores;
        this.numPlayers = numPlayers;
        this.playerNames = playerNames;
        this.is_Judge = is_Judge;
        this.imagesList = new ArrayList<>();
        this.imagesList.addAll(imagesList);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PlayersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.player, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        int color = holder.mTextView.getResources().getColor(R.color.colorPrimaryDark);
        holder.mTextView.setBackgroundColor(color);
        if (is_Judge[position]) {
            holder.mTextView.setBackgroundColor(Color.YELLOW);
        }
        holder.player.setText(playerNames[position]);
        holder.score.setText(Integer.toString(scores[position]));

        //if the network connection was not made, the imagesList will be empty.
        //if it is empty, set the default values.
        if(imagesList.size()==0) {
            if (position == 0) { //the first player should be you.
                holder.player.setText("You");
                Drawable placeHolder = ContextCompat.getDrawable(context, R.drawable.android1);
                holder.bindDrawable(placeHolder);
            }
            else{
                holder.player.setText("Player"); //all other players
                Drawable placeHolder = ContextCompat.getDrawable(context, R.drawable.android1);
                holder.bindDrawable(placeHolder);
            }

        }
        else {
            //connection was made and imagesList contains images
            if (position == 0) { //the first player should be you.
                holder.player.setText("You");
                imageView.setImageBitmap(imagesList.get(0).getBitmapImage());

            } else {
                int j = holder.getAdapterPosition(); //other players
                holder.player.setText(imagesList.get(j).getId());
                imageView.setImageBitmap(imagesList.get(j).getBitmapImage());
            }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return numPlayers;
    }

    public List<Images> getImagesList(){
        return imagesList;
    }
}