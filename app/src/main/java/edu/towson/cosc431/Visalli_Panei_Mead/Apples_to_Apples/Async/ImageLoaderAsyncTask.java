package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.Async;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.GameActivity;

/**
 * Created by Beth on 5/3/2018.
 */

public class ImageLoaderAsyncTask extends AsyncTask<Void, Void, List<Images>> {


    List<Images> imagesList = new ArrayList<>();

    private static final String TAG = "ImageFetcher";

    @Override
    protected List<Images> doInBackground(Void... params) {
        //complete network call and parse JSON object in background thread.
        return new ImageFetcher().fetchItems();
    }

    @Override
    protected void onPostExecute(List<Images> imagesList){
        this.imagesList.addAll(imagesList);
        Log.d(TAG, "IMAGE LOADER ASYNC TASK: " + imagesList.size());
    }
}
