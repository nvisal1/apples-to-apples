package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.Card;

/**
 * Created by randy on 3/29/18.
 */

public class CardsDatabase extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "decks.db";
    private static final int VERSION = 7;
    public Context context;

    private static final String CREATE_TABLE =
            "create table " + DatabaseContract.TABLE_NAME + " ( " +
                    DatabaseContract._ID + " text primary key, " +
                    DatabaseContract.CONTENT_COLUMN + " text, " +
                    DatabaseContract.CATEGORY_COLUMN + " text, " +
                    DatabaseContract.COLOR_COLUMN + " text, " +
                    DatabaseContract.IS_HELD_COLUMN + " boolean, " +
                    DatabaseContract.DELETED_COLUMN + " integer default 0);";

    public CardsDatabase(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
        List<Card> cards = new ArrayList<>();
        // Red Cards
        Card card = new Card(UUID.randomUUID(), "piranha: a fish with very sharp teeth. It has a reputation as a fearsome predator.", "Fun Fact", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(),"Donald Trump: (1946–) US real estate developer, the 45th President of the United States.", "History", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "The Queen of England: Elizabeth II is Queen of the United Kingdom and the other Commonwealth realms.", "History", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Mahatma Gandhi: Indian activist and leader of the Indian independence movement against British rule.", "History", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Back to the Future: (movie 1985) With the help of a wacky scientist, a young teen travels back to 1955 in a Delorean turned time-machine.", "Movies", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "The Oster Toaster: a fabulous toaster that serves your toast with a crunch.", "Fun Fact", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "The IRS: want a tax return?", "Fun Fact", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "The Mailman: did someone say 2-Day delivery?", "Fun Fact", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Cats: a 4-legged animal that is popular to watch on Youtube.", "Nature", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Hiccups: an involuntary spasm of the diaphragm and respiratory organs", "Fun Fact", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Skunks: black and white: striped animals able to produce bad smells", "Nature", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Running out of Toilet Paper: No squares to spare", "OH NO!", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Watching paint dry: No better way to pass the time", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Brad Pitt- American actor in Fight Club, Ocean's Eleven, World War Z, the crush of too many women.", "Movies", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Mark Twain - 1835-1910, American writer and humorist, known for the adventures of Tom Sawyer and Huckleberry Finn", "History", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "A Bad Wi-Fi connection- video buffering...", "OH NO!", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Mustard - Made from seeds of the mustard plant", "Fun  Fact", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Gall Bladder - Where excess bile is stored", "Fun Fact", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Hand Sanitizer- kills 99.9% of germs", "Nature", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Doing your Laundry- Do these socks match?", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "A Bad Haircut - The perfect start to a bad hair day", "OH NO!", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "A Full Moon - \"When the moon hits your eye like a big pizza pie, that's amore!\" -Dean Martin", "OH NO!", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "A Funeral - no jokes here, respect for the dead.", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "A Leaky Boat - Time to bail out.", "OH NO!", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Albert Einstein - 1879-1855, German-born American physicist and Nobel laureate who created theories of relativity. Nice hair.", "History", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Antony & Cleopatra - Roman general and Egyptian queen. A love affair that wouldn't die. Oh, wait, it did.", "History", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Apples & Oranges - Don't even TRY to compare them.", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Atlantis - Mythological city where the folks had that sinking feeling.", "Fiction", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Bankruptcy - It'll be the debt of you.", "OH NO!", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Barbed Wire - Wire with sharp points, used for fences. Created havoc on the western frontier.", "Fun Fact", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Barney - Purple dinosaur. \"I love you, you love me, blah, blah, blah ... \"", "Fiction", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "BB Guns - You'll shoot your eye out!\"", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Beach Balls - Blow them up and watch them blow away! P.S. Amazon sells a 12 footer!", "Fun Fact", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Beer - \"Beer is proof that God loves us and wants us to be happy.\" - Benjamin Franklin", "History", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Beer Bellies - Just think of them as microbreweries.", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Boy Scouts - On my honor, I will do my best... and learn to camp.", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Braces - No more gum, no caramels, no corn on the cob.", "OH NO!", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Brains - Vital to human existence and mad scientists. It's all in your head!", "Fun Fact", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Casinos - Slots and slots of fun.", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Churches - \"Here is the church, and here is the steeple...\"", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Black Socks - They never get dirty. The longer you wear them the stronger they get.", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Doing The Hokey-Pokey - That's what it's all about.", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Doing The Dishes - Come on, we all know they go in the sink for days.", "OH NO!", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Duct Tape - All-purpose, ... all the time ...", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Family Reunions - Just to remind you why you moved away in the first place ...", "OH NO!", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Food Baby - 9 hours of gurgling...", "OH NO!", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Flip-Flops - It's the sound they make when you walk.", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Flowers & Candy - When \"I'm sorry\" is not enough.", "OH NO!", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Fortune Cookies - \"You will soon eat a flat, dry, tasteless cookie . . .\"", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Going To School - Secondary education? It's elementary", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Going To The Dentist - This won't hurt a bit ...", "OH NO!", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Going To The Gym - Who is Jim? What did he ever do to you?", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Yoga - It's just heinies in your face, Yuck!", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Hangnails - We've fingered the cause of the pain.", "OH NO!", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Hitting A Home-Run - Going... going... CRASH... through the neighbor's window!", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Homework - Finally, something to do during commercials, said no one ever.", "OH NO!", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "House Guests - How many visitors does it take to drive you crazy? Go on -take a guest!", "OH NO!", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Jonah & The Whale - The greatest fish story ever told, in the Bible, that is.", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Lemons - When life gives you lemons, make lemonade ... or, in the case of a car, take it back. There's a law, you know!", "Fun Fact", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Llamas - happy llama, sad llama, drama llama, super llama, big fat mama llama.", "Misc", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Looking For A Job - I have student loans, please hire me.", "OH NO!", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Marilyn Monroe - 1926-62, American movie star, pinup and sex symbol. Just ask the Kennedys.", "History", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Midlife Crisis - \"Here comes your 19th nervous breakdown ...\" -The Rolling Stone", "History", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Midnight - Cinderella's curfew.", "Fiction", "RED", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Motorcycles - The air in your hair and bugs in your teeth.", "Misc", "RED", false);
        cards.add(card);
        // Green Cards
        card = new Card(UUID.randomUUID(), "Vitreous: clear, glass-like, see-through", "Fiction", "GREEN", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Obtuse: annoyingly insensitive or slow to understand", "OH NO!", "GREEN", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Erinaceous: spiny, hedgehog-like, small", "Fun Fact", "GREEN", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Mouthwatering: appetizing, delicious, tasty", "Misc", "GREEN", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Wild:  uncivilized, rabid, unpredictable", "Misc", "GREEN", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Creepy: Frightening, slivering, scary", "Fun Fact", "GREEN", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Perplexing: Mind-blowing, confusing, fascinating", "History", "GREEN", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Humble: selfless, modest, meek", "History", "GREEN", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Greasy: Slimy, slippery, oily", "OH NO!", "GREEN", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Attractive: Beautiful, good-looking, pretty", "Fiction", "GREEN", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Roasted: cook, bake, grill, broil", "Fun Fact", "GREEN", false);
        cards.add(card);
        card = new Card(UUID.randomUUID(), "Zany: eccentric, peculiar, odd, unconventional", "Misc", "GREEN", false);
        cards.add(card);

        for (int i = 0; i < cards.size(); i++) {
            ContentValues cv = intoContentValues(cards.get(i));
            db.insert(DatabaseContract.TABLE_NAME, null, cv);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table " + DatabaseContract.TABLE_NAME);
        db.execSQL(CREATE_TABLE);
    }

    // Creates a ContentValue object from the properties of a Card object
    private ContentValues intoContentValues(Card card) {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseContract._ID, card.getId());
        cv.put(DatabaseContract.CONTENT_COLUMN, card.getContent());
        cv.put(DatabaseContract.CATEGORY_COLUMN, card.getCategory());
        cv.put(DatabaseContract.IS_HELD_COLUMN, card.is_held());
        cv.put(DatabaseContract.COLOR_COLUMN, card.getColor());
        return cv;
    }
}
