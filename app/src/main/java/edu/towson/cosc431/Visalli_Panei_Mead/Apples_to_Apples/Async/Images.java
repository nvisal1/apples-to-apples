package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.Async;

import android.graphics.Bitmap;

/**
 * Created by Beth on 5/9/2018.
 */

public class Images {

    private String id; //id is the player name from the JSON object
    private String url; //url is the link to the picture
    private Bitmap bitmap; //imageView will get the bitmap image

    public Images(){
        
    }

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return id;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public String getUrl(){
        return url;
    }

    public void setBitmap(Bitmap bitmap){
        this.bitmap = bitmap;
    }

    public Bitmap getBitmapImage(){
        return bitmap;
    }

}
