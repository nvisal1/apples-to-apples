package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.gamelogic;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.Card;
import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.database.CardsDataStore;
import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.database.IDataStore;

public class GameController {
   // public static GreenCard currentGreen;
    private static boolean win;

    public static List<Card> greenDeck;
    public static List<Card> redDeck;
    public static List<Card> allCards;
    public static List<Card> hand;

    public Card getCurrentGreen() {
        return currentGreen;
    }

    public Card currentGreen;

    private IDataStore dataStore;
    //private static Human human;
    private static int numPlayers = 4;	//number of players-- get from settings
    private static int winCondition;	//how many green cards for a player to win-- get from settings

    public GameController (IDataStore datastore, int numPlayers, int winCondition) {
        this.redDeck = new ArrayList<>();
        this.greenDeck = new ArrayList<>();
        this.hand = new ArrayList<>();
        this.dataStore = datastore;
        this.numPlayers = numPlayers;
        this.winCondition = winCondition;
        this.allCards = datastore.getCards();

        initializeDecks(allCards);
        setHand();
        Log.d("CONSTRUCTOR", hand.toString());
    }

    public void setHand() {	//each player in the player list draws 7 random cards from the deck
        // Draw seven random red cards for initial hand
        for (int i = 0; i < 7; i++) {
            int randomNum = 0 + (int)(Math.random() * (((redDeck.size() - 1) - 0) + 1));
            while(redDeck.get(randomNum).is_held()) {
                randomNum = 0 + (int)(Math.random() * (((redDeck.size() - 1) - 0) + 1));
            }
            redDeck.get(randomNum).setIs_held(true);
            hand.add(redDeck.get(randomNum));
        }
    }

    public List<Card> getHand() {
        return hand;
    }

    public void initializeDecks(List<Card>allCards) {
        // split all cards into two decks (red and green)
        for (int i = 0; i < allCards.size(); i++) {

            if (allCards.get(i).getColor().equals("RED")) {
                redDeck.add(allCards.get(i));
            } else {
                greenDeck.add(allCards.get(i));
            }
        }
    }

    public Card drawGreenCard() {
        // Pick a random green card and display it
        int greenIndex = 0 + (int)(Math.random() * (((greenDeck.size() - 1) - 0) + 1));
        this.currentGreen = greenDeck.get(greenIndex);
        return this.currentGreen;
    }



    public List<Card> getSelectedCards(int position) {
        List<Card> selectedCards = new ArrayList<>();

        // Save user-selected
        setHand();
        selectedCards.add(hand.get(position));
        Log.d("MY CARD:", selectedCards.get(0).toString());


        // Bots pick cards
        for (int i = 1; i < numPlayers; i++) {
            int randomNum = 1 + (int)(Math.random() * (((redDeck.size() - 1) - 1) + 1));
            while (redDeck.get(randomNum).is_held()) {
                randomNum = 1 + (int)(Math.random() * (((redDeck.size() - 1) - 1) + 1));
            }
            selectedCards.add(redDeck.get(randomNum));
        }

        Log.d("BOT CARD:", selectedCards.get(1).toString());
        return selectedCards;
    }

    public void isNotHeld() {
        for (int i = 0; i < redDeck.size(); i++) {
            redDeck.get(i).setIs_held(false);
        }
    }
}