package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.gamelogic;
//
//public class RedCard {
//    private String name, description;
//    private Player owner;
//    private boolean inDeck;
//
//    //Constructor
//    public RedCard(String name, String description) {
//        this.name = name;
//        this.description = description;
//        this.owner = null;
//        this.inDeck = true;
//    }
//
//    //ToString
//    @Override
//    public String toString() {
//        return "RedCard [name=" + name + ", description=" + description + ", owner=" + owner + ", inDeck=" + inDeck
//                + "]";
//    }
//
//    //GETTERS AND SETTERS
//    public String getName() {
//        return name;
//    }
//    public void setName(String name) {
//        this.name = name;
//    }
//    public String getDescription() {
//        return description;
//    }
//    public void setDescription(String description) {
//        this.description = description;
//    }
//    public Player getOwner() {
//        return owner;
//    }
//    public void setOwner(Player owner) {
//        this.owner = owner;
//    }
//    public boolean isInDeck() {
//        return inDeck;
//    }
//    public void setInDeck(boolean inDeck) {
//        this.inDeck = inDeck;
//    }
//}
