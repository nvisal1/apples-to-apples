package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples;

import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;

import java.io.IOException;
import java.util.UUID;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SETTINGS_CODE = 76; //used for settings menu intent
    private static final int GAME_CODE = 101; //used for gameplay intent

    Game settings = new Game (2, 3, false, "English"); //Default settings

    TextView autoString;
    int count = 0;
    TextView loadingTV;
    //Menu buttons
    Button playBtn;
    Button settingsBtn;
    Button howToPlayBtn;

    //Game data to fetch from settings
    Game game;
    boolean check = true;


    //String[] autoStringValues = getResources().getStringArray(R.array.auto_string_values);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        howToPlayBtn = findViewById(R.id.howToPlayBtn);
        howToPlayBtn.setOnClickListener(this);

        final String[] autoStringValues = getResources().getStringArray(R.array.auto_string_values);
        loadingTV = findViewById(R.id.loadingTV);

        //setup widgets
        playBtn = findViewById(R.id.playBtn);
        settingsBtn = findViewById(R.id.settingsBtn);

        //setup onclick
        playBtn.setOnClickListener(this);
        settingsBtn.setOnClickListener(this);


        autoString = findViewById(R.id.autoString);

        /*
            This thread downloads all of the cards from an api.
            The JSON objects are parsed to Card objects.
        */

        /*new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    todoList = JsonDownloader.downloadTodos("1");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                for (int i = 0; i < todoList.size(); i++) {
                    dataStore.addTodo(todoList.get(i));
                }
            }
        }).start();*/

        /*
            ==========================
             TEST
            ==========================
        */

        /*
            This thread controls the auto-updating string that is located under the title
            on the main menu
        */
        Thread t = new Thread() {
            @Override
            public void run() {
                while (!isInterrupted()) {
                    try {
                        Thread.sleep(10000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                count++;
                                if (count > autoStringValues.length - 1) {
                                    count = 0;
                                }

                                // if (count %2 == 0) {
                                //     autoString.setTextColor(Color.parseColor("#ff99cc00"));
                                // } else {
                               // autoString.setTextColor(Color.parseColor("#ffff4444"));
                                // }
                                autoString.setText(autoStringValues[count]);

                            }
                        });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        t.start();

       // viewAll();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.settingsBtn:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivityForResult(intent, SETTINGS_CODE);
                break;
            case R.id.playBtn:
                playBtn.setVisibility(View.INVISIBLE);
                settingsBtn.setVisibility(View.INVISIBLE);
                loadingTV.setVisibility(View.VISIBLE);
                Intent intentPlay = new Intent(this, GameActivity.class);
                intentPlay.putExtra("Number of Players", settings.getNumPlayers());
                intentPlay.putExtra("Score Limit", settings.getNumRounds());
                startActivityForResult(intentPlay, GAME_CODE);
                break;
            case R.id.howToPlayBtn:
                InstructionsFragment fragment;
                FragmentManager fragmentManager = getSupportFragmentManager();

                // ALWAYS try to find an existing fragment
                fragment = (InstructionsFragment) fragmentManager.findFragmentByTag("FRAGMENT");

                if (check) {
                    playBtn.setVisibility(View.INVISIBLE);
                    settingsBtn.setVisibility(View.INVISIBLE);
                    if (fragment == null) {
                        // only add a fragment if it hasn't been added before
                        fragment = new InstructionsFragment();
                        fragmentManager.beginTransaction()
                                .add(R.id.fragment_container, fragment, "FRAGMENT")
                                .commit();
                    }
                    howToPlayBtn.setText("I Got It!");
                    check = false;
                } else {
                    playBtn.setVisibility(View.VISIBLE);
                    settingsBtn.setVisibility(View.VISIBLE);
                    fragmentManager.beginTransaction()
                            .remove(fragment)
                            .commit();
                    howToPlayBtn.setText("How To Play");
                    check = true;
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("MAIN ACTIVITY", "IN ACTIVITY RESULT");
        if(requestCode == SETTINGS_CODE) {
            if (resultCode == RESULT_OK) {
                //Handle settings results
                int numPlayers = data.getIntExtra(SettingsActivity.NUM_PLAYERS_KEY, 2);
                int numRounds = data.getIntExtra(SettingsActivity.NUM_ROUNDS_KEY, 2);
                String language = data.getStringExtra(SettingsActivity.LANGUAGE_KEY);
                boolean funny = data.getBooleanExtra(SettingsActivity.FUNNY_KEY, false);

                //New Game
                settings = new Game(numPlayers, numRounds, funny, language); //update default settings

                // Log the game for now TODO: DO SOMETHING WITH THE GAME
                Log.d("MAIN ACTIVITY", "NEW GAME WITH SETTINGS: " + settings.toString());
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        playBtn.setVisibility(View.VISIBLE);
        settingsBtn.setVisibility(View.VISIBLE);
        loadingTV.setVisibility(View.INVISIBLE);
    }
}

