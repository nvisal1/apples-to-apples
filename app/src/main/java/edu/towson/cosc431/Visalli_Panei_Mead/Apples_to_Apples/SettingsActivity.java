package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;

/**
 * Created by Anthony on 3/4/2018.
 */

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener{


    public static final String NUM_PLAYERS_KEY = "NUM_PLAYERS";
    public static final String NUM_ROUNDS_KEY = "NUM_ROUNDS";
    public static final String FUNNY_KEY = "GAME_FUNNY";
    public static final String LANGUAGE_KEY = "GAME_LANGUAGE";

    Spinner numPlayers;
    Spinner numRounds;
    CheckBox funny;
    Spinner language;
    Button submitBtn;
    Button backBtn;

    int resultNumPlayers, resultNumRounds;
    String resultLanguage;
    boolean resultFunny;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        numPlayers = findViewById(R.id.numPlayersSpnr);
        numRounds = findViewById(R.id.numRoundsSpnr);
//        funny = findViewById(R.id.funnyCb);
//        language = findViewById(R.id.languageSpnr);
        submitBtn = findViewById(R.id.submitBtn);
        backBtn = findViewById(R.id.defaultBtn);

        submitBtn.setOnClickListener(this);
        backBtn.setOnClickListener(this);

        ArrayAdapter<CharSequence> playerAdapter = ArrayAdapter.createFromResource(this, R.array.players_array, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> roundAdapter = ArrayAdapter.createFromResource(this, R.array.rounds_array, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> languageAdapter = ArrayAdapter.createFromResource(this, R.array.language_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        playerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        roundAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        languageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        numPlayers.setAdapter(playerAdapter);
        numRounds.setAdapter(roundAdapter);
//        language.setAdapter(languageAdapter);

        //Fetch spinner results
        numPlayers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                String selectedItem = parent.getItemAtPosition(position).toString();
                try{
                    resultNumPlayers = Integer.parseInt(selectedItem);
                }catch(Exception e){

                }
            }
            public void onNothingSelected(AdapterView<?> parent)
            {
                    resultNumPlayers = 2;
            }
        });

        numRounds.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                String selectedItem = parent.getItemAtPosition(position).toString();
                try{
                    resultNumRounds = Integer.parseInt(selectedItem);
                }catch(Exception e){

                }
            }
            public void onNothingSelected(AdapterView<?> parent)
            {
                resultNumRounds = 2;
            }
        });

//        language.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
//        {
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
//            {
//                String selectedItem = parent.getItemAtPosition(position).toString();
//                resultLanguage = selectedItem;
//            }
//            public void onNothingSelected(AdapterView<?> parent)
//            {
//                resultLanguage = "English";
//            }
//        });

    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.submitBtn:
                Intent resultIntent = new Intent();
                resultIntent.putExtra(NUM_PLAYERS_KEY, resultNumPlayers);
                resultIntent.putExtra(NUM_ROUNDS_KEY, resultNumRounds);
                resultIntent.putExtra(LANGUAGE_KEY, resultLanguage);
//                resultIntent.putExtra(FUNNY_KEY, funny.isChecked());

                // 2b. Set the result
                setResult(RESULT_OK, resultIntent);

                // 3. Finish the activity
                finish();
                break;
            case R.id.defaultBtn:
                numPlayers.setSelection(0);
                numRounds.setSelection(0);
//                funny.setChecked(false);
//                language.setSelection(0);
                //finish();
                break;
        }
    }
}
