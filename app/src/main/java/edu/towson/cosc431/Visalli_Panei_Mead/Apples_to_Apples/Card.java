package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples;

import java.util.UUID;

/**
 * Created by anthonyvisalli on 4/15/18.
 */

public class Card {
    private UUID id;
    private String content;
    private String category;
    private String color;

    @Override
    public String toString() {
        return "Card{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", category='" + category + '\'' +
                ", color='" + color + '\'' +
                ", is_held=" + is_held +
                '}';
    }

    public String getId() {
        return id.toString();
    }

    public void setId(String id) {
        this.id = UUID.fromString(id);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getColor() { return color; }

    public void setColor(String color) { this.color = color; }

    public boolean is_held() {
        return is_held;
    }

    public void setIs_held(boolean is_held) {
        this.is_held = is_held;
    }

    public Card(UUID id, String content, String category, String color, boolean is_held) {

        this.id = id;
        this.content = content;
        this.category = category;
        this.color = color;
        this.is_held = is_held;
    }

    public Card() {
        id = UUID.randomUUID();
    }

    private boolean is_held;
}
