package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples;

/**
 * Created by Anthony on 3/4/2018.
 */

public class Game {

    private int numPlayers;
    private int numRounds;
    private boolean funny;
    private String language;

    public Game(int numPlayers, int numRounds, boolean funny, String language){
        this.numPlayers = numPlayers;
        this.numRounds = numRounds;
        this.funny = funny;
        this.language = language;
    }

    @Override
    public String toString() {
        return "Game Settings:{" +
                "numPlayers='" + numPlayers + '\'' +
                ", numRounds='" + numRounds + '\'' +
                ", language=" + language +
                ", isFunny=" + funny +
                '}';

    }

    public int getNumPlayers() {
        return numPlayers;
    }

    public void setNumPlayers(int numPlayers) {
        this.numPlayers = numPlayers;
    }

    public int getNumRounds() {
        return numRounds;
    }

    public void setNumRounds(int numRounds) {
        this.numRounds = numRounds;
    }

    public boolean isFunny() {
        return funny;
    }

    public void setFunny(boolean funny) {
        this.funny = funny;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

}
