package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.database;

import java.util.List;
import java.util.UUID;

import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.Card;

/**
 * Created by randy on 3/29/18.
 */

public interface IDataStore {
    List<Card> getCards();
    void addCard(Card card);
    //void deleteCard(Card card); We never want to delete cards!
    void updateCard(Card card);
    Card getCardById(UUID uuid);
}
