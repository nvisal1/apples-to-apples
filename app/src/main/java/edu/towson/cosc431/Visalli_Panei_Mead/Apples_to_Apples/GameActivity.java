package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.Async.ImageLoaderAsyncTask;
import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.Async.Images;
import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.database.CardsDataStore;
import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.gamelogic.GameController;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;

/**
 * Created by anthonyvisalli on 3/2/18.
 */

public class GameActivity extends AppCompatActivity implements CardViewHolder.OnViewClickedListener {

    private RecyclerView mRecyclerView;
    private RecyclerView cRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.Adapter cAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.LayoutManager cLayoutManager;
    int numPlayers;
    int scoreLimit;
    CardsDataStore dataStore;
    List<Card> allCards;
    List<Card> redDeck;
    List<Card> greenDeck;
    List<Card> hand;
    int[] scores;
    String[] playerNames;
    TextView judgeCard;
    boolean[] isJudge;
    int numCards;
    public List<Images> imagesList = new ArrayList<>();
    private Handler handler;
    private ThreadPoolExecutor threadPoolExecutor;

    ImageLoaderAsyncTask asyncTask;

    static Context context;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        asyncTask = new ImageLoaderAsyncTask();
        asyncTask.execute();
        try {
            imagesList= asyncTask.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        GameActivity.context = getApplicationContext();
        // a UI handler
        handler = new Handler();
        threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(1);


        judgeCard = findViewById(R.id.judgeText);

        numCards = 7;

        redDeck = new ArrayList<>();
        greenDeck = new ArrayList<>();
        hand = new ArrayList<>();
        Intent intent = getIntent();
        numPlayers = intent.getIntExtra("Number of Players", 2);
        scoreLimit = intent.getIntExtra("Score Limit", 3);
        Log.d("SCORE LIMIT", Integer.toString(scoreLimit));

        scores = new int[numPlayers];
        playerNames = new String[] {"You", "CK Droid",  "Millennial Droid", "Linux Droid"};

        isJudge = new boolean[numPlayers];

        // Player 2 is always first judge
        isJudge = new boolean[] {false, true, false, false};

        // Initial game set up
        dataStore = new CardsDataStore(this);
        // Constructor will create first hand, so just get hand here.
        GameController gameController = new GameController(dataStore, numPlayers, scoreLimit);
        hand = gameController.getHand();
        Card greenCard = gameController.drawGreenCard();
        judgeCard.setText(greenCard.getContent());
        setInitalScore();

        mRecyclerView = findViewById(R.id.recyclerView);
        cRecyclerView = findViewById(R.id.recyclerView2);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        cRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        cLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        cRecyclerView.setLayoutManager(cLayoutManager);

        // animators
        cRecyclerView.setItemAnimator(new SlideInLeftAnimator());

        // specify an adapter (see also next example)
        mAdapter = new PlayersAdapter(numPlayers, scores, playerNames, isJudge, imagesList);
        mRecyclerView.setAdapter(mAdapter);
        
        cAdapter = new CardsAdapter(cRecyclerView, hand, gameController, this, numCards);
        cRecyclerView.setAdapter(cAdapter);

    }

    private void setInitalScore() {
        // Initialize scores to 0
        for (int i = 0; i < numPlayers; i++) {
            scores[i] = 0;
        }
    }

    @Override
    public void onViewClickedListener(final int position, final GameController gameController) {

        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Presentation);
        } else {
            builder = new AlertDialog.Builder(this);
        }

        builder.setTitle(hand.get(position).getCategory())
                .setMessage(hand.get(position).getContent())
                .setPositiveButton("Submit Card", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continuation of game is triggered (Bot turns are automatically taken)
                        List<Card> selectedCards = gameController.getSelectedCards(position);
                        Card currentGreen = gameController.getCurrentGreen();

                        // Find bot judge and remove their card from selected
                        int restrictedCard = 1;
                        for (int i = 0; i < isJudge.length; i++) {
                            if (isJudge[i]) {
                                restrictedCard = i;
                            }
                        }

                        int randomNum = getWinner(selectedCards, restrictedCard, currentGreen);
                        boolean userIsJudge = userIsJudge(randomNum);
                        // Update view with current scores and judge status
                        mRecyclerView.setAdapter(mAdapter);
                        // Set all card is_held booleans to false
                        gameController.isNotHeld();
                        if (userIsJudge) {
                            hand.clear();
                            hand = gameController.getSelectedCards(position);
                            // Remove user card
                            hand.remove(0);
                            numCards = hand.size();
                            cAdapter = new CardsAdapter(cRecyclerView, hand, gameController, GameActivity.this, numCards);
                            cRecyclerView.setAdapter(cAdapter);
                            Card greenCard = gameController.drawGreenCard();
                            judgeCard.setText(greenCard.getContent());
                        } else {
                            // Redistribute cards and update view
                            hand.clear();
                            gameController.setHand();
                            hand = gameController.getHand();
                            numCards = 7;
                            cAdapter = new CardsAdapter(cRecyclerView, hand, gameController, GameActivity.this, numCards);
                            cRecyclerView.setAdapter(cAdapter);
                            cAdapter.notifyDataSetChanged();
                            Card greenCard = gameController.drawGreenCard();
                            judgeCard.setText(greenCard.getContent());
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }

    private boolean userIsJudge(int randomNum) {
        // Set new judge
        for(int i = 0 ; i < numPlayers; i++) {
            isJudge[i] = false;
        }
        isJudge[randomNum] = true;

        // If user becomes judge, set layout accordingly
        if (isJudge[0]) {
            return true;
        }
        return false;
    }

    private int getWinner(List<Card> selectedCards, int restrictedCard, Card currentGreen) {
        // Pick a winner and increment their score by 1!

//        random winner
//        int randomNum = 0 + (int)(Math.random() * (((numPlayers - 1) - 0) + 1));
//        while (randomNum == restrictedCard) {
//            randomNum = 0 + (int)(Math.random() * (((numPlayers - 1) - 0) + 1));
//        }

//      winner by category match
        List matchingCategories = new ArrayList();
        int roundWinner;
        for (int i = 0; i < selectedCards.size(); i++) {
            if (selectedCards.get(i).getCategory().equals(currentGreen.getCategory())) {
                matchingCategories.add(i);
            }
        }
        if (matchingCategories.size() > 1) {
            int multipleMatchIndex = 0 + (int)(Math.random() * (((matchingCategories.size() - 1) - 0) + 1));
            roundWinner = Integer.parseInt(matchingCategories.get(multipleMatchIndex).toString());
            while (roundWinner == restrictedCard) {
                multipleMatchIndex = 0 + (int)(Math.random() * (((matchingCategories.size() - 1) - 0) + 1));
                roundWinner = Integer.parseInt(matchingCategories.get(multipleMatchIndex).toString());
            }
        } else if (matchingCategories.size() == 1) {
            roundWinner = Integer.parseInt(matchingCategories.get(0).toString());
            while (roundWinner == restrictedCard) {
                roundWinner = 0 + (int)(Math.random() * (((numPlayers - 1) - 0) + 1));
            }
        } else {
            roundWinner = 0 + (int)(Math.random() * (((numPlayers - 1) - 0) + 1));
            while (roundWinner == restrictedCard) {
                roundWinner = 0 + (int)(Math.random() * (((numPlayers - 1) - 0) + 1));
            }
        }

        Log.d("RANDOM NUMBER:", Integer.toString(roundWinner));
        scores[roundWinner] += 1;

        // Check for end of game!
        for (int i = 0; i < numPlayers ; i++) {
            if (scores[i] == scoreLimit) {
                displayEndGame(i);
            }
        }

        // Display winning card
        displayWinner(selectedCards, roundWinner);
        return roundWinner;
    }

    private void displayEndGame(int i) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Presentation);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Match Winner!")
                .setMessage("Congratulations " + playerNames[i])
                .setPositiveButton("Return to Menu", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }

    private void displayWinner(List<Card> selectedCards, int index) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Presentation);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        for (int i = 0; i < selectedCards.size(); i++) {
            Log.d("SELECTED CARD " + i, selectedCards.get(i).getContent());
        }
        // Only show card content when the user doesn't pick the winner!
        if (isJudge[0]) {
            builder.setTitle("Round Winner: " + playerNames[index])
                    .setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .show();
        } else {
            builder.setTitle("Round Winner: " + playerNames[index])
                    .setMessage(selectedCards.get(index).getContent())
                    .setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .show();
        }
    }
}
