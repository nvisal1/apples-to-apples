package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.gamelogic;

//
//public class GreenCard {
//    private String name, description;
//    private Player owner;
//    private boolean inDeck;
//    public String[] tags;
//    private int funnyval, rank;	//possible values for bots to use?
//
//    //Constructor
//    public GreenCard(String name, String description, String[] tags) {
//        this.name = name;
//        this.description = description;
//        this.tags = tags;
//        this.owner = null;
//        this.inDeck = true;
//    }
//
//    //Getters and Setters
//    public String getName() {
//        return name;
//    }
//    public void setName(String name) {
//        this.name = name;
//    }
//    public String getDescription() {
//        return description;
//    }
//    public void setDescription(String description) {
//        this.description = description;
//    }
//    public Player getOwner() {
//        return owner;
//    }
//    public void setOwner(Player owner) {
//        this.owner = owner;
//    }
//    public boolean isInDeck() {
//        return inDeck;
//    }
//    public void setInDeck(boolean inDeck) {
//        this.inDeck = inDeck;
//    }
//
//    public int getFunnyval() {
//        return funnyval;
//    }
//
//    public void setFunnyval(int funnyval) {
//        this.funnyval = funnyval;
//    }
//
//    public int getRank() {
//        return rank;
//    }
//
//    public void setRank(int rank) {
//        this.rank = rank;
//    }
//}