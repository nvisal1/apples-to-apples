package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.Async;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Beth on 4/23/2018.
 */

public class ImageFetcher {


    private static final String TAG = "JSON Server";
    public List<Images> imagesList = new ArrayList<>();
    Bitmap bitmap;

    public ImageFetcher() {
    }

    //given a url, open connection
    public byte[] getUrlBytes(String urlSpec) throws IOException {

        BufferedReader reader = null;
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try{
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();
            if(connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() + ": with " + urlSpec);
            }
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while((bytesRead = in.read(buffer))>0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }

    //method to get url string
    public String getUrlString(String urlSpec) throws IOException {
        return new String(getUrlBytes(urlSpec));
    }

    //fetch JSON object
    public List<Images> fetchItems(){
        InputStream inputStream = null;
        try{
            String url = Uri.parse("https://my-json-server.typicode.com/bmead410/demo/db")
                    .buildUpon()
                    .appendQueryParameter("format", "json")
                    .appendQueryParameter("nojsoncallback", "1")
                    .appendQueryParameter("extras", "url_s")
                    .build().toString();
            String jsonString = getUrlString(url);
            Log.d("TAG", "Recieved JSON " + jsonString);
            JSONObject jsonObject = new JSONObject(jsonString);
            //call parseItems to get JSON object and images
            parseItems(imagesList, jsonObject);



        } catch (IOException e) {
            Log.d("TAG", "Fails to fetch items", e);
        } catch (JSONException e) {
            Log.d(TAG, "Failed to parse JSON" + e);
        }

        Log.d(TAG, "IMAGE FETCHER: " + imagesList.size());
        return imagesList;
    }

    //parses Json object and set image to the appropriate
    private void parseItems(List<Images> imagesList, JSONObject jsonObject)throws IOException, JSONException{
        JSONObject imagesJsonObject = jsonObject.getJSONObject("images");
        JSONArray imageJsonArray = imagesJsonObject.getJSONArray("image");
       // Log.d(TAG, "Image Object?" + imageJsonArray.get(0).toString()); //check if its parsing

        //parse object to initialize image object
        for(int i=0; i<imageJsonArray.length(); i++){
            JSONObject imageJsonObject = imageJsonArray.getJSONObject(i);
            Log.d(TAG, "Image Object?" + imageJsonArray.get(i).toString());
            Images image = new Images();
            image.setId(imageJsonObject.getString("id"));

            if(!imageJsonObject.has("location")){
                Log.d(TAG, "No location " + imageJsonObject.toString());
                continue;
            }
            image.setUrl(imageJsonObject.getString("location"));

            Bitmap bitmap = null;
            //try the image url and set the bitmap image
            try{
                    String imageUrl = image.getUrl();
                    InputStream input = new URL(imageUrl).openStream();
                    bitmap = BitmapFactory.decodeStream(input);
                    image.setBitmap(bitmap);
                    Log.d("IMAGE - ", "i- " + imagesList.get(i).getUrl());

            }catch(Exception e){
                //catch error
                Log.d("BITMAP ERROR: ", "Can't add " + image.getUrl());
            }


            imagesList.add(image);

            Log.d(TAG, "Image size: " + imagesList.size());
        }
    }
}
