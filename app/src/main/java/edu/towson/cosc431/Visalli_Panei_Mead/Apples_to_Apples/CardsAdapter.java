package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.database.IDataStore;
import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.gamelogic.GameController;

/**
 * Created by anthonyvisalli on 3/22/18.
 */

public class CardsAdapter extends RecyclerView.Adapter<CardViewHolder> {
    RecyclerView rv;
    List<Card> hand = new ArrayList<>();
    GameController gameController;
    int numCards;

    private final CardViewHolder.OnViewClickedListener mListener;
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View mTextView;
        public TextView card;
        public ViewHolder(View v) {
            super(v);
            mTextView = v;
            card = v.findViewById(R.id.textPlayer);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CardsAdapter(RecyclerView rv, List<Card>hand, GameController gameController, CardViewHolder.OnViewClickedListener mListener, int numCards) {
        this.rv = rv;
        // this.deck = deck;
        this.hand.addAll(hand);
        this.mListener = mListener;
        this.gameController = gameController;
        this.numCards = numCards;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cards, parent, false);

        CardViewHolder vh = new CardViewHolder(v, mListener, this.gameController);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        /*
            Take the first 7 cards from deck and append their category to recyclerView.
            It should always be first 7 cards because the are removed from deck at end of turn!
        */

       // holder.card.setText(this.deck.get(position).getCategory());
        holder.mTextView.setBackgroundColor(Color.WHITE);
        holder.bindCardToView(this.hand.get(position));

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.numCards;
    }
}