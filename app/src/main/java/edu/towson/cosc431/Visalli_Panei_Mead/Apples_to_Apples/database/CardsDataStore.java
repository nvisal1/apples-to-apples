package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.Card;

/**
 * Created by randy on 3/29/18.
 */

public class CardsDataStore implements IDataStore {

    private CardsDatabase helper;

    public CardsDataStore(Context ctx) {
        helper = new CardsDatabase(ctx);
    }

    @Override
    public List<Card> getCards() {
        SQLiteDatabase db = this.helper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + DatabaseContract.TABLE_NAME, null);
        List<Card> cards = new ArrayList<>();
        while(cursor.moveToNext()) {
            Card card = new Card();
            String id = cursor.getString(cursor.getColumnIndex(DatabaseContract._ID));
            String content = cursor.getString(cursor.getColumnIndex(DatabaseContract.CONTENT_COLUMN));
            boolean isHeld = cursor.getInt(cursor.getColumnIndex(DatabaseContract.IS_HELD_COLUMN))
                    == 1;
            String category = cursor.getString(cursor.getColumnIndex(DatabaseContract.CATEGORY_COLUMN));
            String color = cursor.getString(cursor.getColumnIndex(DatabaseContract.COLOR_COLUMN));
            card.setId(id);
            card.setIs_held(isHeld);
            card.setContent(content);
            card.setCategory(category);
            card.setColor(color);
            cards.add(card);
            Log.d("GET CARD", card.getCategory());
        }
        return cards;
    }

    @Override
    public void addCard(Card card) {
        SQLiteDatabase db = this.helper.getWritableDatabase();
        ContentValues cv = intoContentValues(card);
        Log.d("ADD CARD", cv.getAsString("color"));
        db.insert(DatabaseContract.TABLE_NAME, null, cv);
    }

    @Override
    public void updateCard(Card card) {
        ContentValues contentValues = intoContentValues(card);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.update(DatabaseContract.TABLE_NAME, contentValues, DatabaseContract._ID + " =?", new String[] {card.getId()});
    }

    @Override
    public Card getCardById(UUID uuid) {
        SQLiteDatabase db = this.helper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + DatabaseContract.TABLE_NAME, null);
        Card cards = null;
        while(cursor.moveToNext()) {
            Card card = new Card();
            String id = cursor.getString(cursor.getColumnIndex(DatabaseContract._ID));

            if (id.equals(uuid.toString())) {
                card = new Card();
                String content = cursor.getString(cursor.getColumnIndex(DatabaseContract.CONTENT_COLUMN));
                boolean isHeld = cursor.getInt(cursor.getColumnIndex(DatabaseContract.IS_HELD_COLUMN))
                        == 1;
                String category = cursor.getString(cursor.getColumnIndex(DatabaseContract.CATEGORY_COLUMN));
                String color = cursor.getString(cursor.getColumnIndex(DatabaseContract.COLOR_COLUMN));
                card.setId(id);
                card.setIs_held(isHeld);
                card.setContent(content);
                card.setCategory(category);
                card.setColor(color);
                return card;
            }
        }
        return cards;
    }

    // Creates a ContentValue object from the properties of a Card object
    private ContentValues intoContentValues(Card card) {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseContract._ID, card.getId());
        cv.put(DatabaseContract.CONTENT_COLUMN, card.getContent());
        cv.put(DatabaseContract.CATEGORY_COLUMN, card.getCategory());
        cv.put(DatabaseContract.IS_HELD_COLUMN, card.is_held());
        cv.put(DatabaseContract.COLOR_COLUMN, card.getColor());
        return cv;
    }
}
