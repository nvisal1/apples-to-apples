package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.database;

import android.provider.BaseColumns;

/**
 * Created by randy on 3/29/18.
 */

public class DatabaseContract implements BaseColumns {
    public static final String TABLE_NAME = "Red_Deck";
    public static final String _ID = "id";
    public static final String CONTENT_COLUMN = "content";
    public static final String CATEGORY_COLUMN = "category";
    public static final String COLOR_COLUMN = "color";
    public static final String IS_HELD_COLUMN = "is_held";
    public static final String DELETED_COLUMN = "deleted";

}
