package edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples;

import android.app.Activity;
import android.app.Instrumentation;
import android.graphics.Color;
import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.Async.ImageFetcher;
import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.database.CardsDataStore;
import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.database.IDataStore;
import edu.towson.cosc431.Visalli_Panei_Mead.Apples_to_Apples.gamelogic.GameController;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasBackground;
import static android.support.test.espresso.matcher.ViewMatchers.hasTextColor;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;

/**
 * Created by anthonyvisalli on 5/10/18.
 */
public class MainActivityTest {

    private MainActivity mActivity = null;
    Instrumentation.ActivityMonitor gameMonitor;
    Instrumentation.ActivityMonitor settingMonitor;
    Instrumentation.ActivityMonitor mainMonitor;



    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    @Before
    public void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();
    }

    @Test
    public void testLaunch() {
        View view = mActivity.findViewById(R.id.playBtn);
        assertNotNull(view);
    }

    @Test
    public void testLaunchOfPlayBtn() {
        gameMonitor = getInstrumentation().addMonitor(GameActivity.class.getName(), null, false);
        assertNotNull(mActivity.findViewById(R.id.playBtn));
        onView(withId(R.id.playBtn)).perform(click());

        Activity gameActivity = gameMonitor.waitForActivity();
        assertNotNull(gameActivity);
    }


    @Test
    public void testPlayersChangedOnSettingChange(){
        Game settings = new Game (3, 3, false, "English");
        assertEquals(3, settings.getNumPlayers());
    }

    @Test
    public void testRoundsChangedOnSettingChange(){
        Game settings = new Game (3, 3, false, "English");
        assertEquals(3, settings.getNumRounds());
    }

    @Test
    public void testSelectedOnSubmitClick(){
        gameMonitor = getInstrumentation().addMonitor(GameActivity.class.getName(), null, false);
        onView(withId(R.id.playBtn)).perform(click());
        GameActivity gActivity = (GameActivity) gameMonitor.waitForActivity();
        CardsDataStore dataStore = new CardsDataStore(gActivity);
        GameController controller = new GameController(dataStore, 2, 3);
        onView(withId(R.id.recyclerView2)).perform(click());
        //onView(withText("Submit")).perform(click());
        assertEquals(2, controller.getSelectedCards(0).size());
    }
    //Test that if you hit cancel, selected cards does not contain that card
    @Test
    public void testCardRecyclerView(){
        gameMonitor = getInstrumentation().addMonitor(GameActivity.class.getName(), null, false);
        onView(withId(R.id.playBtn)).perform(click());
        onView(withId(R.id.recyclerView2)).perform(click());
    }

    @Test
    public void testHand() {
        gameMonitor = getInstrumentation().addMonitor(GameActivity.class.getName(), null, false);
        onView(withId(R.id.playBtn)).perform(click());
        GameActivity gActivity = (GameActivity) gameMonitor.waitForActivity();
        CardsDataStore dataStore = new CardsDataStore(gActivity);
        GameController controller = new GameController(dataStore, 2, 3);
        assertEquals(7, controller.getHand().size());
    }

    @Test
    public void testGetCurrentGreenCard() {
        gameMonitor = getInstrumentation().addMonitor(GameActivity.class.getName(), null, false);
        onView(withId(R.id.playBtn)).perform(click());
        GameActivity gActivity = (GameActivity) gameMonitor.waitForActivity();
        CardsDataStore dataStore = new CardsDataStore(gActivity);
        GameController controller = new GameController(dataStore, 2, 3);
        controller.drawGreenCard();
        assertNotNull(controller.getCurrentGreen());
    }

    @Test
    public void testFirstJudgeIsPlayerTwo(){
        gameMonitor = getInstrumentation().addMonitor(GameActivity.class.getName(), null, false);
        onView(withId(R.id.playBtn)).perform(click());
        // onView(withId(R.id.recyclerView)).check()
        GameActivity gActivity = (GameActivity) gameMonitor.waitForActivity();
        assertTrue(gActivity.isJudge[1]);
    }

    @Test
    public void testInitialScoreIsZero(){
        gameMonitor = getInstrumentation().addMonitor(GameActivity.class.getName(), null, false);
        onView(withId(R.id.playBtn)).perform(click());
        // onView(withId(R.id.recyclerView)).check()
        GameActivity gActivity = (GameActivity) gameMonitor.waitForActivity();
        assertEquals(0, gActivity.scores[0]);
        assertEquals(0, gActivity.scores[1]);
    }

    @Test
    public void testCheckPlayerNames(){
        gameMonitor = getInstrumentation().addMonitor(GameActivity.class.getName(), null, false);
        onView(withId(R.id.playBtn)).perform(click());
        // onView(withId(R.id.recyclerView)).check()
        GameActivity gActivity = (GameActivity) gameMonitor.waitForActivity();
        assertEquals("You", gActivity.playerNames[0]);
        assertEquals("CK Droid", gActivity.playerNames[1]);
        assertEquals("Millennial Droid", gActivity.playerNames[2]);
        assertEquals("Linux Droid", gActivity.playerNames[3]);
    }

    @Test
    public void testJSONIfNull(){
        gameMonitor = getInstrumentation().addMonitor(GameActivity.class.getName(), null, false);
        onView(withId(R.id.playBtn)).perform(click());
        // onView(withId(R.id.recyclerView)).check()
        GameActivity gActivity = (GameActivity) gameMonitor.waitForActivity();
        ImageFetcher imageFetcher = new ImageFetcher();
        assertNotNull(imageFetcher.fetchItems());
    }

    @Test
    public void testLoadingGameTV(){
        gameMonitor = getInstrumentation().addMonitor(GameActivity.class.getName(), null, false);
        onView(withId(R.id.loadingTV)).check(matches(not(isDisplayed())));
    }

    @Test
    public void testPlayerRecyclerView(){
        gameMonitor = getInstrumentation().addMonitor(GameActivity.class.getName(), null, false);
        onView(withId(R.id.playBtn)).perform(click());
        onView(withId(R.id.recyclerView2)).check(matches(isDisplayed()));
    }

    @Test
    public void testGreenCard(){
        gameMonitor = getInstrumentation().addMonitor(GameActivity.class.getName(), null, false);
        onView(withId(R.id.playBtn)).perform(click());
        onView(withId(R.id.judgeText)).check(matches(isDisplayed()));
    }



    //Test that if you win you become the judge

    //Test if player is judge, that player's hand size = numPlayers - 1



    @After
    public void tearDown() throws Exception {
        mActivity = null;
        gameMonitor = null;
        settingMonitor = null;
        mainMonitor = null;
    }



}