READ ME 

APPLES TO APPLES 

1. Settings
	Modify settings as desired to play with more bots or increase the goal score. 
	The default has 2 players with 3 as the goal to win. 
2. How to Play
	This is the fragment that displays the instructions to the game. 
3. Play
	By pressing the Play button, the game is started. The judge is highlighted in yellow and the players 
	are highlighted in blue. The user's hand has 7 cards and by clicking on them, they can submit the card
	to the judge who will pick the winning card. The winner's score will increment and they will become the 
	judge. When the user is the judge, they will receive the cards from the bots. They can choose the 
	winner, and the game continues. 